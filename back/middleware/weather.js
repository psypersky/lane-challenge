const request = require('axios');
const log = require('loglevel');
const { buildWeatherEndpoint } = require('../config/openWeather');

module.exports = function weatherFactory() {
  return async function weather(ctx, next) {
    if (!ctx.request.geoLocation) {
      log.warn('skipping weather, no geoLocation');
      await next();
      return;
    }

    try {
      const { lat, lon } = ctx.request.geoLocation;
      const { data } = await request.get(buildWeatherEndpoint(lat, lon));
      log.debug('got weather info', data);
      ctx.request.weather = data;
    } catch (e) {
      log.error('[Error] calling weather api', e.message);
      if (e.response) {
        log.error('[Error] weather api status', e.response.status);
        log.error('[Error] weeather api res:', e.response.data);
      }
    }

    await next();
  };
};
