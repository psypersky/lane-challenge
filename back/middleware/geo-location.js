const request = require('axios');
const log = require('loglevel');

module.exports = function geoLocationFactory() {
  return async function geoLocation(ctx, next) {
    let ip = ctx.request.ip;

    // For the propuse of testing
    if (ip === '127.0.0.1') {
      ip = '189.181.236.35';
    }

    log.debug('request with ip', ip);

    try {
      const { data } = await request.get(`http://ip-api.com/json/${ip}`);
      log.debug('got ip-api response', data);
      ctx.request.geoLocation = {
        lat: data.lat,
        lon: data.lon,
      };
    } catch (e) {
      log.error('calling ip-api', e.message);
      if (e.response) {
        log.error('ip-api status', e.respose.status);
        log.error('ip-api res:', e.respose.data);
      }
    }

    await next();
  };
};
