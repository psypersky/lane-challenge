const weatherEndpoint = 'http://api.openweathermap.org/data/2.5/weather';
const apiKey = process.env.OPEN_WEATHER_API_KEY;

const buildWeatherEndpoint = (lat, lon) => `${weatherEndpoint
  }?lat=${lat}&lon=${lon}&APPID=${apiKey}`;

module.exports = {
  buildWeatherEndpoint,
};
