
module.exports = async function someRoute(ctx) {
  const res = {
    geoLocation: ctx.request.geoLocation,
    weather: ctx.request.weather,
  };

  ctx.response.body = {
    status: 'success',
    data: res,
  };
  ctx.response.status = 200;
};
