require('../env/loadenv');

const log = require('loglevel');
log.setLevel('trace');

const app = require('./server');
const serverConfig = require('../config/server');


log.info('serverConfig', serverConfig);

app.listen(serverConfig.port, '0.0.0.0', (e) => {
  if (e) {
    log.error('on server listening', e.message, e.stack);
    return;
  }
  log.info('==> 🌎 Listening on port %s.', serverConfig.port);
});
