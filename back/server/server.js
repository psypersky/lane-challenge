const Koa = require('koa');
const Router = require('koa-router');
const geoLocationMiddleware = require('../middleware/geo-location');
const weatherMiddleware = require('../middleware/weather');
const someRoute = require('../api/someRoute');

const app = new Koa();
const router = new Router();

// Geo-location middleate
app.use(geoLocationMiddleware());

// Weather middleware
app.use(weatherMiddleware());

// Some route
router.get('/some-route', someRoute);

// Server app router
app.use(router.routes()).use(router.allowedMethods());

module.exports = app;
