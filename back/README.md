
### Running the project
__Use node 7 for async await support__  

`npm i`   
`npm i -g nodemon`   
`npm run dev`

### Running tests
Install mocha first   
`npm i -g mocha`

Run tests   
`npm test`

### Linter

If you have problems install these as globals   
`npm i -g eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react`

### Comments
Native Async Await yeiii
