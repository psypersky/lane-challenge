/**
 * Read .env file and put it in process.env
 * If .env does not exists, `.env.default` will be used instead.
 * variables not defined in .env will take the values of .env.default
 */
const fs = require('fs');
const path = require('path');
const dotenv = require('dotenv');
const log = require('loglevel');

try {
  fs.statSync(path.resolve(__dirname, '.env'));
  log.debug('Loading env config');
  dotenv.load({ path: path.resolve(__dirname, '.env') });
} catch (e) {
  log.warn('no .env config, using just .env.default');
}

dotenv.load({ path: path.resolve(__dirname, '.env.default') });
