require('../env/loadenv');

const expect = require('chai').expect;
const log = require('loglevel');
const request = require('supertest');
const server = require('../server/server');
const serverConfig = require('../config/server');

log.setLevel('trace');

describe('Server', () => {
  let app;

  before((done) => {
    app = server.listen(serverConfig.port, '0.0.0.0', (e) => {
      if (e) {
        log.error('on server listening', e.message, e.stack);
        done(e);
        return;
      }
      log.info('==> 🌎 Listening on port %s.', serverConfig.port);
      done();
    });
  });

  describe('some route', () => {
    it('should return ip geo-location', (done) => {
      request(app)
        .get('/some-route')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((e, res) => {
          if (e) return done(e);
          log.debug('got response', res.body);
          expect(res.body.status).to.equal('success');
          expect(res.body.data.geoLocation).to.be.an('Object');
          expect(res.body.data.weather).to.be.an('Object');
          done();
        });
    });
  });
});
