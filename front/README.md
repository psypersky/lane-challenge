# Lane Front End Test

### Run the project

Install dependencies   
`npm i`

Run project for development   
`npm run dev`

Go to `localhost:3000` if it doesn't open automatically

### Linter
`npm i -g eslint`

### Notes

There are two ways of doing this, maintain the order on which each transformation was done or just assuming an order to avoid different results by different order of pressing the buttons, i chose to always maintaining one order.

Normally we would maintain a "dump" component but since we are using canvas i stored the state on the component.

The project was so small that there was no necessity of using cerebral.js chains and signals.

I suck at design u.u
