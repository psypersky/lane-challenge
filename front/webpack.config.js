const webpack = require('webpack');
const path = require('path');

const plugins = [
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
    },
  }),
];

if (process.env.NODE_ENV === 'production') {
  plugins.push(new webpack.optimize.UglifyJsPlugin(
    { compress: { warnings: false } }));
}

module.exports = {
  resolve: {
    extensions: ['.js', '.jsx', '.css', '.scss'],
  },
  entry: path.resolve('src', 'main.js'),
  output: {
    path: path.resolve('build'),
    publicPath: '/build',
    filename: 'bundle.js',
  },
  devServer: {
    port: 3000,
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['react', 'es2015', 'stage-0'],
      },
    }, {
      // scss styles are loaded with modules local scope
      test: /\.scss$/,
      loader: 'style-loader!css-loader?modules&localIdentName=' +
        '[local]---[hash:base64:5]!sass-loader',
    }],
  },
  plugins,
};
