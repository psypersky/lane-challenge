import React from 'react';
import { connect } from 'cerebral-view-react';
import DropZone from 'react-dropzone';
import log from 'loglevel';
import styles from './styles.scss';

const CANVAS_WIDTH = 500;
const CANVAS_HEIGHT = 500;

export default connect({
},
  class App extends React.Component {

    constructor(props) {
      super(props);

      this.state = {
        image: null,
        rotated: false,
        translated: false,
        dimmed: false,
        scaled: false,
      };

      this.globalAlpha = 1;
    }

    handleDropImage = (files) => {
      const file = files[0];

      const imageType = /^image\//;
      if (!imageType.test(file.type)) {
        alert('Please select only images');
        return;
      }

      const image = new Image();
      image.src = URL.createObjectURL(file);

      log.debug('image source', image.src);

      image.onload = () => {
        this.setState({ image });
      };
    }

    drawImage = (ctx) => {
      log.debug('drawing image');

      const { image, rotated, translated, scaled, dimmed } = this.state;

      ctx = ctx || this.canvas.getContext('2d');

      ctx.save();

      ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

      if (rotated) {
        ctx.translate(CANVAS_WIDTH / 2, CANVAS_WIDTH / 2);
        ctx.rotate(45 * (Math.PI / 180));
        ctx.translate(-CANVAS_WIDTH / 2, -CANVAS_WIDTH / 2);
      }

      if (translated) {
        ctx.translate(-40, 0);
      }

      if (scaled) {
        ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        ctx.scale(0.5, 0.5);
      }

      if (dimmed) {
        ctx.globalAlpha = 0.5;
      }

      if (image) {
        const widthRatio = CANVAS_WIDTH / image.width;
        const heightRatio = CANVAS_HEIGHT / image.height;
        const imgRatio = widthRatio < heightRatio ? widthRatio : heightRatio;
        const imgWidth = image.width * imgRatio;
        const imgHeight = image.height * imgRatio;
        ctx.drawImage(image, 0, 0, imgWidth, imgHeight);
      }

      ctx.restore();
    }

    componentDidUpdate() {
      this.drawImage();
    }

    handleRotateClick = () => {
      const rotated = this.state.rotated;

      this.setState({ rotated: !rotated });
    }

    handleTranslateClick = () => {
      log.debug('translating');
      this.setState({ translated: !this.state.translated });
    }

    handleScaleClick = () => {
      log.debug('scaling');
      this.setState({ scaled: !this.state.scaled });
    }

    handleOpacityClick = () => {
      log.debug('dimming');
      this.setState({ dimmed: !this.state.dimmed });
    }

    handleResetClick = () => {
      log.debug('resetting');
      this.setState({
        rotated: false,
        translated: false,
        scaled: false,
        dimmed: false,
        image: null,
      });
    }

    render() {
      const { image, rotated, translated, scaled, dimmed } = this.state;

      return (
        <div className={styles.wrapper}>
          <div className={styles.containerLeft}>
            <div>
              <canvas
                className={styles.canvas}
                ref={canvas => this.canvas = canvas}
                width={CANVAS_WIDTH}
                height={CANVAS_HEIGHT}
              />
            </div>
          </div>
          <div className={styles.containerRight}>
            <DropZone onDrop={this.handleDropImage} className={styles.dropZone}>
              <div className={styles.dropZoneContent}>
                Try dropping some files here,
                or click to select files to upload.
              </div>
            </DropZone>
            <div>
              <button
                className={rotated ? styles.buttonGreen : styles.buttonRed}
                disabled={!image}
                onClick={this.handleRotateClick}
              >
                Rotate
              </button>
            </div>

            <div>
              <button
                className={translated ? styles.buttonGreen : styles.buttonRed}
                disabled={!image}
                onClick={this.handleTranslateClick}
              >
                Translate
              </button>
            </div>

            <div>
              <button
                className={scaled ? styles.buttonGreen : styles.buttonRed}
                disabled={!image}
                onClick={this.handleScaleClick}
              >
                Scale
              </button>
            </div>

            <div>
              <button
                className={dimmed ? styles.buttonGreen : styles.buttonRed}
                disabled={!image}
                onClick={this.handleOpacityClick}
              >
                Opacity
              </button>
            </div>

            <div>
              <button
                className={styles.buttonReset}
                disabled={!image}
                onClick={this.handleResetClick}
              >
                Reset
              </button>
            </div>
          </div>
        </div>
      );
    }
  },
);
