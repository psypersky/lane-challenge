import { Controller } from 'cerebral';
import Devtools from 'cerebral-module-devtools';
import log from 'loglevel';
import model from './model';

log.setLevel('trace');

const controller = Controller(model);

controller.addSignals({
});

controller.addModules({
  devtools: Devtools(),
});

export default controller;
